# CloudFormation

Test project with:

* **Language:** YAML / JSON
* **Framework:** CloudFormation

## Content Attribution

The example CloudFormation templates were copied from https://github.com/awslabs/aws-cloudformation-templates @ 2415d1dd34bdbf50e3b009879f6ba754a043afdf

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                | :white_check_mark: OR :x: |
| Dependency Scanning | :x: |
| Container Scanning  | :x: |
| DAST                | :x: |
| License Management  | :x: |
